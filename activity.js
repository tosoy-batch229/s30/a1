// Aggregate to count the total number of items supplied by Yellow Farms and has a price less than 50.
db.fruits.aggregate([
    {$match: {
        $and:[
            {price: {$lt: 50}},{supplier: "Yellow Farms"}
        ]
    }},
    {$count: "totalFruitsByYellowFarmLessThan50"}
])


// Aggregate to count the total number of items with price lesser than 30.
db.fruits.aggregate([
    {$match: {price: {$lt: 30}}},
    {$count : "lessThan30Price"}
])

// Aggregate to get the average price of fruits supplied by Yellow Farm.
db.fruits.aggregate([
    {$match: {supplier: "Yellow Farms"}},
    {$group: {_id: "FruitsSuppliedByYellowFarms", avgPrice: {$avg: "$price"}}}
])


// Aggregate to get the highest price of fruits supplied by Red Farm Inc.
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "highestPrice", maxPrice: {$max : "$price"}}}
])

// Aggregate to get the cheapest price of fruits supplied by Red Farm Inc.
db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "lowestPrice", minPrice: {$min : "$price"}}}
])